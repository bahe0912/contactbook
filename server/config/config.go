package config

import (
	"github.com/joho/godotenv"
	"log"
	"os"
)

func Init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func GetEnvVariable(key string) string {
	return os.Getenv(key)
}
