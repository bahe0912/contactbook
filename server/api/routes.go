package api

import (
	"context"
	"log"

	"net/http"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/bahe0912/contactbook/server/db"
	"gitlab.com/bahe0912/contactbook/server/models"
)

// GetContacts retrieves all contacts from MongoDB
func GetContacts(c *fiber.Ctx) error {
	var contacts []models.Contact

	// Find all contacts in the MongoDB collection
	cursor, err := db.Collection.Find(context.TODO(), bson.M{})
	if err != nil {
		log.Printf("Error finding contacts: %v", err)
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{"error": "Failed to retrieve contacts"})
	}
	defer cursor.Close(context.TODO())

	// Decode and append each contact to the result slice
	for cursor.Next(context.TODO()) {
		var contact models.Contact
		if err := cursor.Decode(&contact); err != nil {
			log.Printf("Error decoding contact: %v", err)
			return c.Status(http.StatusInternalServerError).JSON(fiber.Map{"error": "Failed to retrieve contacts"})
		}
		contacts = append(contacts, contact)
	}

	return c.JSON(contacts)
}

// AddContact adds a new contact to the MongoDB
func AddContact(c *fiber.Ctx) error {
	var contact models.Contact

	// Parse request body into the contact struct
	if err := c.BodyParser(&contact); err != nil {
		return c.Status(http.StatusBadRequest).JSON(fiber.Map{"error": err.Error()})
	}

	// Insert the contact into the MongoDB collection
	_, err := db.Collection.InsertOne(context.TODO(), contact)
	if err != nil {
		log.Printf("Error inserting contact: %v", err)
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{"error": "Failed to add contact"})
	}

	return c.Status(http.StatusCreated).JSON(contact)
}

func SetupRoutes(app *fiber.App) {
	app.Get("/api/getContacts", GetContacts)
	app.Post("/api/addContact", AddContact)
}
