package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// Contact represents a contact record.
type Contact struct {
	ID           primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name         string             `json:"name,omitempty"`
	Email        string             `json:"email,omitempty"`
	Phone        string             `json:"phone,omitempty"`
	Organization string             `json:"organisation"`
	Address      string             `json:"address,omitempty"`
	Image        string             `json:"image,omitempty"`
}
