package main

import (
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"

	"gitlab.com/bahe0912/contactbook/server/api"
	"gitlab.com/bahe0912/contactbook/server/config"
	"gitlab.com/bahe0912/contactbook/server/db"
)

func main() {
	// Initialize the application configuration.
	config.Init()

	// Initialize the database connection.
	db.Init()

	// Create a Fiber app
	app := fiber.New()

	// Use the CORS middleware to enable Cross-Origin Resource Sharing.
	app.Use(cors.New(cors.Config{
		AllowOrigins:     "http://localhost:5173", // Update this with your React app's origin.
		AllowMethods:     "GET,POST,PUT,DELETE",
		AllowHeaders:     "Content-Type, Authorization",
		ExposeHeaders:    "Authorization",
		AllowCredentials: true,
	}))

	// Define routes
	api.SetupRoutes(app)

	// Start the server
	port := config.GetEnvVariable("PORT")
	if port == "" {
		port = ":8080"
	}
	go func() {
		log.Fatal(app.Listen(port))
	}()

	log.Printf("Server started on port %s", port)
	select {}
}
