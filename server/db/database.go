package db

import (
	"context"
	"fmt"
	"log"
	"time"

	"gitlab.com/bahe0912/contactbook/server/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var Collection *mongo.Collection

// Init initializes the MongoDB connection.
func Init() {
	mongoURI := config.GetEnvVariable("MONGODB_URI")
	if mongoURI == "" {
		log.Fatal("MONGO_URI environment variable is not set.")
	}

	clientOptions := options.Client().ApplyURI(mongoURI)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	Collection = client.Database("Contactdb").Collection("Contacts")

	fmt.Println("Connected to MongoDB")
}
