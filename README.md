# Project README

<img src="./ReadmeAssets/logo.png" alt="Project Logo" width="100" height="100" />

## Project Overview

This project is a web application that utilizes modern technologies and follows best practices in both frontend and backend development. It's designed to manage contacts and provides various functionalities for users. Below, we'll outline the key features and technologies used in this project.

### Getting Started

To run this project, follow these steps:

## Prerequisites

Before you can run this project, you need to have the following dependencies installed:

- Node.js
- Go

## Installation

### Node.js

If you don't have Node.js installed on your system, follow these steps to install it:

1. **Windows:**

   - Visit the [Node.js official website](https://nodejs.org/).
   - Download the LTS (Long Term Support) version for Windows.
   - Run the installer and follow the on-screen instructions.

2. **macOS:**

   - You can use [Homebrew](https://brew.sh/) for installation.
     ```sh
     brew install node
     ```

3. **Linux (Debian/Ubuntu):**

   - You can use `apt` for installation.
     ```sh
     sudo apt update
     sudo apt install nodejs
     ```

4. **Verify Installation:**
   - Open your terminal or command prompt and run the following commands to check the Node.js and npm (Node Package Manager) versions:
     ```sh
     node -v
     npm -v
     ```

### Go

If you don't have Go installed on your system, follow these steps to install it:

1. **Windows, macOS, Linux:**

   - Visit the [Go Downloads page](https://golang.org/dl/).
   - Download the installer for your operating system.
   - Run the installer and follow the on-screen instructions.

2. **Verify Installation:**
   - Open your terminal or command prompt and run the following command to check the Go version:
     ```sh
     go version
     ```

That's it! You should now have Node.js and Go installed on your system.

1. Clone the project repository to your local machine.
   Make sure to use the `main` branch of this repository for the latest stable version of the project.

2. Change your directory to the `client` folder and run `yarn add` to install dependencies.
3. Run `yarn run dev` to start the frontend.
4. In another terminal tab, change your directory to the `server` folder and run `go run .` to start the backend (**add period for current directory**).

Ensure you have MongoDB set up and configured correctly for the backend.

### Technologies Used

- Frontend:
  - [React](https://reactjs.org/) with TypeScript: Building a robust and type-safe frontend.
  - [Tailwind CSS](https://tailwindcss.com/): Ensuring responsive and visually appealing UI components.
  - [React Query](https://react-query.tanstack.com/) and [Axios](https://axios-http.com/): Efficiently managing remote state and handling API requests.
- Backend:
  - [Go](https://golang.org/) and [Fiber](https://gofiber.io/): Creating a high-performance and efficient backend server.
  - [MongoDB](https://www.mongodb.com/): Storing and retrieving data from a NoSQL database.

## Design

View the design in Figma: [Figma Design File](https://www.figma.com/file/hzWBafQEoRdEEh2Q4NgKXQ/AddressBook_Zoho?type=design&node-id=0%3A1&mode=design&t=Oml0U9J0mAuYGWcx-1).

### Folder Structure

Both the frontend and backend components follow a well-organized folder structure for maintainability and scalability.

![Folder Structure React](./ReadmeAssets/react-folder.jpg)
![Folder Structure Go](./ReadmeAssets/go-folder.jpg)

### Contact List Page

The home page of the website is the contact list page, where users can view their list of contacts. Notable features include:

- **Loading Spinner:** A spinner is displayed until all the contact data is fetched completely.
- **Default Image:** If a profile image is missing or broken, a default image is displayed.
- **Search Functionality:** Users can search for contacts by name.
- **Alphabetic Sorting:** Contact cards are sorted alphabetically.
- **Fully Responsive:** The page is responsive across different devices.

![Add Contacts](./ReadmeAssets/Contact.gif)

### Adding Contacts

Adding contacts is made easy with a popup form that includes various validations and feedback mechanisms:

- **React Hook Form:** Handling form inputs efficiently.
- **Zod for Validation:** Validating each field before submission.
- **Base64 Image:** Converting profile images to Base64 format for better practices.
- **Custom Hook and Context API:** State Mangement.
- **Loading Spinner:** A loading spinner is displayed in the submit button during submission.
- **Contact Saved Notification:** Users receive a notification when the contact is saved successfully using [React Toastify](https://fkhadra.github.io/react-toastify/introduction/).

![Contact List Page](./ReadmeAssets/Forms.gif)

### Testing

For the best experience, try running the application with low throttling in the network tab of your browser to see the minor interactions.

![Loading](./ReadmeAssets/Loading.jpg)

This project handles errors gracefully to provide a smooth user experience.
