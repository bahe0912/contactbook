/* eslint-disable @typescript-eslint/no-explicit-any */
import React, {
  createContext,
  useContext,
  useReducer,
  ReactNode,
  Reducer,
} from 'react';

type State = {
  isPopUpOpen: boolean;
  searchQuery: string;
};

type Action = {
  type: string;
  payload?: any;
};

type ContextType = {
  state: State;

  handlers: any;
};

const InitialState: State = {
  isPopUpOpen: false,
  searchQuery: '',
};

const reducer: Reducer<State, Action> = (state, action) => {
  switch (action.type) {
    case 'popUp':
      return { ...state, isPopUpOpen: action.payload };
    case 'search':
      return { ...state, searchQuery: action.payload };
    default:
      return state;
  }
};

const FormContext = createContext<ContextType | undefined>(undefined);

type FormContextProviderProps = {
  children: ReactNode;
};

function FormContextProvider({ children }: FormContextProviderProps) {
  const [state, dispatch] = useReducer(reducer, InitialState);

  const handlers = {
    setPopUp: function (bool: boolean) {
      dispatch({ type: 'popUp', payload: bool });
    },
    setSearchQuery: function (query: string) {
      dispatch({ type: 'search', payload: query });
    },
  };

  return (
    <FormContext.Provider value={{ state, handlers }}>
      {children}
    </FormContext.Provider>
  );
}

// Custom hook
function usePopUp() {
  const context = useContext(FormContext);
  if (context === undefined) {
    throw new Error('useBook must be used within a FormContextProvider');
  }
  return context;
}

export { FormContextProvider, usePopUp };
