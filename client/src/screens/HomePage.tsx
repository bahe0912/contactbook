import { Main, SideBar } from '../Components';

function HomePage() {
  return (
    <main className="bg-bg w-full h-[100vh] grid grid-cols-1 sm:grid-cols-layout font-Fira ">
      <SideBar className="col-start-1 col-end-2 bg-primary" />
      <Main className="col-start-2 col-end-3 bg-bg " />
    </main>
  );
}

export default HomePage;
