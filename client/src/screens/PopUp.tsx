import PopUpLeft from '../Components/PopUpLeft';
import PopUpRight from '../Components/PopUpRight';
import { usePopUp } from '../Context/FormContext';

function PopUp() {
  const { state } = usePopUp();
  return (
    <>
      <div
        className={`fixed inset-0 flex items-center justify-center shadow-popup transition-opacity ease-in ${
          state?.isPopUpOpen ? 'opacity-100' : 'opacity-0 pointer-events-none'
        }`}
      >
        <div className="fixed inset-0 transition-opacity">
          <div className="absolute inset-0 bg-black opacity-30"></div>
        </div>

        <div className="z-50 relative  bg-bg p-4 rounded-lg shadow-lg w-[80vw] xl:w-[70vw] 2xl:w-[50vw] h-[80vh]  grid grid-cols-1 md:grid-cols-popup-layout lg:grid-cols-popup-lg xl:grid-cols-popup-xl 2xl:grid-cols-popup-2xl">
          <PopUpLeft className="bg-primary hidden md:flex shadow-xl rounded-md" />
          <PopUpRight />
        </div>
      </div>
    </>
  );
}

export default PopUp;
