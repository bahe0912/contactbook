import { usePopUp } from '../Context/FormContext';
import AddressForm from './AddressForm';

interface popUpRightPropsType {
  className?: string;
}

function PopUpRight(props: popUpRightPropsType) {
  const { handlers } = usePopUp();
  return (
    <div className={props?.className}>
      <button
        onClick={() => handlers?.setPopUp(false)}
        className="absolute top-2 right-2 text-bgMain hover:text-primary hover:bg-bgMain bg-primary rounded-full w-8 h-8 mr-3 mt-3 flex justify-center items-center transition-c"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-5 w-5"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="3"
            d="M6 18L18 6M6 6l12 12"
          />
        </svg>
      </button>

      <div className="mx-3 my-2 flex w-full h-full justify-center items-center">
        <AddressForm />
      </div>
    </div>
  );
}

export default PopUpRight;
