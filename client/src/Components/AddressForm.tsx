/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { z } from 'zod';
import Button from './Button';
import { zodResolver } from '@hookform/resolvers/zod';
import { useMutation } from 'react-query';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { usePopUp } from '../Context/FormContext';

const AddressSchema = z.object({
  _id: z.string().optional(),
  name: z.string().min(5),
  email: z.string().email().min(1),
  phone: z.string().refine((value) => value.length === 10, {
    message: 'Enter the valid number',
  }),
  organisation: z.string().max(10),
  address: z.string().min(10),
  image: z.string(),
});

export type addressFormType = z.infer<typeof AddressSchema>;

function AddressForm() {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors, isSubmitting },
    control,
  } = useForm<addressFormType>({
    resolver: zodResolver(AddressSchema),
  });
  const { state, handlers } = usePopUp();
  const [imagePreview, setImagePreview] = useState<string | null>(null);
  const [base64Image, setBase64Image] = useState<string | null>('');

  const mutation = useMutation((formData) =>
    axios.post('http://127.0.0.1:8080/api/addContact', formData)
  );

  const onSubmit = async (data: any) => {
    data.image = base64Image;
    try {
      const response = await mutation.mutateAsync(data);
      toast.success('Contact added successfully');
      reset();
      setImagePreview('');
      handlers?.setPopUp(false);
    } catch (error: any) {
      toast.error(`Error: ${error.message}`);
    }
  };

  const handleImagePreview = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      const imageUrl = URL.createObjectURL(file);
      setImagePreview(imageUrl);

      // Convert the selected image to base64 and store it.
      const reader = new FileReader();
      reader.onload = (e) => {
        setBase64Image(e.target?.result as string);
      };
      reader.readAsDataURL(file);
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)} className="font-Lato w-[80%]">
        <div className="mb-3">
          <label htmlFor="name" className="block font-bold mb-2">
            <span className="after:content-['*'] after:ml-0.5 after:text-red-500 block text-md font-medium text-label">
              Name
            </span>
          </label>
          <input
            {...register('name')}
            type="text"
            id="name"
            name="name"
            className={`w-full px-3 py-2 bg-inputfield leading-tight border rounded-lg focus:outline-none focus:shadow-outline-blue focus:border-blue-300 ${
              errors.name ? 'bg-red-300' : 'bg-inputfield'
            } `}
          />
          {errors.name && (
            <p className="text-xs font-light text-error mt-2">{`${errors.name.message}`}</p>
          )}
        </div>

        <div className="mb-3">
          <label htmlFor="email" className="block font-bold mb-2">
            <span className="after:content-['*'] after:ml-0.5 after:text-red-500 block text-md font-medium text-label">
              Email Address
            </span>
          </label>
          <input
            {...register('email')}
            type="email"
            id="email"
            name="email"
            className={`w-full px-3 py-2 bg-inputfield leading-tight border rounded-lg focus:outline-none focus:shadow-outline-blue focus:border-blue-300 ${
              errors.email ? 'bg-red-300' : 'bg-inputfield'
            } `}
          />
          {errors.email && (
            <p className="text-xs font-light text-error mt-2">{`${errors.email.message}`}</p>
          )}
        </div>

        <div className="mb-3">
          <label htmlFor="phone" className="block  font-bold mb-2">
            <span className="after:content-['*'] after:ml-0.5 after:text-red-500 block text-md font-medium text-label">
              Phone Number
            </span>
          </label>
          <input
            {...register('phone')}
            type="number"
            id="phone"
            name="phone"
            className={`w-full px-3 py-2 bg-inputfield leading-tight border rounded-lg focus:outline-none focus:shadow-outline-blue focus:border-blue-300 appearance-none ${
              errors.phone ? 'bg-red-300' : 'bg-inputfield'
            } `}
          />
          {errors.phone && (
            <p className="text-xs font-light text-error mt-2">{`${errors.phone.message}`}</p>
          )}
        </div>

        <div className="mb-3">
          <label htmlFor="organisation" className="block  font-bold mb-2">
            <span className=" block text-md font-medium text-label">
              Organisation
            </span>
          </label>
          <input
            {...register('organisation')}
            type="text"
            id="organisation"
            name="organisation"
            className={`w-full px-3 py-2 bg-inputfield leading-tight border rounded-lg focus:outline-none focus:shadow-outline-blue focus:border-blue-300 appearance-none ${
              errors.organisation ? 'bg-red-300' : 'bg-inputfield'
            } `}
          />
          {errors.organisation && (
            <p className="text-xs font-light text-error mt-2">{`${errors.organisation.message}`}</p>
          )}
        </div>

        <div className="mb-3">
          <label htmlFor="address" className="block  font-bold mb-2">
            <span className="after:content-['*'] after:ml-0.5 after:text-red-500 block text-md font-medium text-label">
              Address
            </span>
          </label>
          <input
            {...register('address')}
            type="text"
            id="address"
            name="address"
            className={`w-full h-[130px] px-3 py-2 bg-inputfield leading-tight border rounded-lg focus:outline-none focus:shadow-outline-blue focus:border-blue-300 ${
              errors.address ? 'bg-red-300' : 'bg-inputfield'
            } `}
          />
          {errors.address && (
            <p className="text-xs font-light text-error mt-2">{`${errors.address.message}`}</p>
          )}
        </div>

        <div className="mb-5 flex">
          {imagePreview && (
            <img
              src={imagePreview}
              className="w-[30px] h-[30px] rounded-[50%] mr-2"
            />
          )}
          <Controller
            name="image"
            control={control}
            rules={{
              required: 'Image is required',
            }}
            render={({ field }) => (
              <input
                {...field}
                accept="image/*"
                onChange={(e) => {
                  field.onChange(e);
                  handleImagePreview(e);
                }}
                type="file"
                className="block  text-inputfield file:text-primary file:border-none file:bg-transparent"
              />
            )}
          />
          {errors.image && (
            <p className="text-xs font-light text-error mt-2">{`${errors.image.message}`}</p>
          )}
        </div>

        <Button
          type="submit"
          label="Submit"
          loading={isSubmitting}
          serve="form"
        />
      </form>
    </>
  );
}

export default AddressForm;
