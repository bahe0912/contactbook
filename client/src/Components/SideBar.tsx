interface sideBarProps {
  className?: string;
}

function SideBar(props: sideBarProps) {
  return <aside className={props?.className}></aside>;
}

export default SideBar;
