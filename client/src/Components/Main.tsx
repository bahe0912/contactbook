import Button from './Button';
import SearchBar from './SearchBar';
import ContactsList from './ContactsList';
import PopUp from '../screens/PopUp';

interface mainProps {
  className?: string;
}

function Main(props: mainProps) {
  return (
    <>
      <section
        className={`${props?.className} grid grid-rows-list grid-cols-list justify-center mx-4 my-4 sm:mr-4`}
      >
        <Button
          className="col-start-3 col-end-4  self-center ml-5"
          label="Add Contact"
        />
        <SearchBar className="col-start-1 col-end-4 flex justify-center self-center" />
        <ContactsList className="col-start-1 col-end-4  row-start-4 " />
      </section>
      <PopUp />
    </>
  );
}

export default Main;
