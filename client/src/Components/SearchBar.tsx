import { usePopUp } from '../Context/FormContext';

interface searchBarProps {
  className?: string;
}

function SearchBar(props: searchBarProps) {
  const { handlers } = usePopUp();

  return (
    <div className={`${props?.className}`}>
      <div className="relative">
        <input
          type="text"
          className="w-[90vw] h-[45px] sm:w-[636px] sm:h-[48px] border-gray-300 bg-bgMain  px-5 pr-10 rounded-[10px] text-sm focus:outline-none drop-shadow-lg block"
          placeholder="Search..."
          onChange={(e) => handlers.setSearchQuery(e.target.value)}
        />
        <button className=" p-2 m-1.5 text-primary rounded-lg focus:outline-none hover:text-btnHover absolute right-0 inset-y-0 ">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={2.5}
            stroke="currentColor"
            className="w-5 h-5"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
            />
          </svg>
        </button>
      </div>
    </div>
  );
}

export default SearchBar;
