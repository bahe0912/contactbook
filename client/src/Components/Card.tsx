import { addressFormType } from './AddressForm';
import defaultImage from '../assets/DefaultImage.png';

interface cardPropsType {
  contact: addressFormType;
}

function Card(props: cardPropsType) {
  return (
    <div className="bg-bgMain hover:bg-primary hover:text-bg drop-shadow-lg  rounded-lg overflow-hidden h-48 shadow-md items-center transition-colors ease-in duration-280 hover:scale-104">
      <div className="m-1 justify-center p-4 h-full grid grid-cols-cardInner ">
        <div className="overflow-hidden mr-4 h-full flex justify-center">
          <img
            src={props.contact.image || defaultImage}
            alt=""
            className="w-[88%] h-[60%] sm:w-[90%] sm:h-[48%] md:w-[100%] md:h-[45%] lg:w-[75%] lg:h-[50%]  xl:w-[80%] xl:h-[65%]object-cover rounded-full"
          />
        </div>

        <div className="">
          <h1 className="font-Lato text-2xl font-normal mb-2 ">
            {props?.contact?.name}
          </h1>
          <p className="text-sm font-light mb-2">{props?.contact?.email}</p>
          <p className="text-sm font-light mb-2">{props?.contact?.address}</p>

          <div className="flex">
            {props?.contact?.organisation && (
              <div className="flex mr-4 ">
                <svg
                  className="w-[15px] h-[15px] text-primary"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M12 21a9.004 9.004 0 008.716-6.747M12 21a9.004 9.004 0 01-8.716-6.747M12 21c2.485 0 4.5-4.03 4.5-9S14.485 3 12 3m0 18c-2.485 0-4.5-4.03-4.5-9S9.515 3 12 3m0 0a8.997 8.997 0 017.843 4.582M12 3a8.997 8.997 0 00-7.843 4.582m15.686 0A11.953 11.953 0 0112 10.5c-2.998 0-5.74-1.1-7.843-2.918m15.686 0A8.959 8.959 0 0121 12c0 .778-.099 1.533-.284 2.253m0 0A17.919 17.919 0 0112 16.5c-3.162 0-6.133-.815-8.716-2.247m0 0A9.015 9.015 0 013 12c0-1.605.42-3.113 1.157-4.418"
                  />
                </svg>

                <p className="text-xs font-light ml-2">
                  {props?.contact?.organisation}
                </p>
              </div>
            )}

            <div className="flex">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-[15px] h-[15px] text-primary"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M2.25 6.75c0 8.284 6.716 15 15 15h2.25a2.25 2.25 0 002.25-2.25v-1.372c0-.516-.351-.966-.852-1.091l-4.423-1.106c-.44-.11-.902.055-1.173.417l-.97 1.293c-.282.376-.769.542-1.21.38a12.035 12.035 0 01-7.143-7.143c-.162-.441.004-.928.38-1.21l1.293-.97c.363-.271.527-.734.417-1.173L6.963 3.102a1.125 1.125 0 00-1.091-.852H4.5A2.25 2.25 0 002.25 4.5v2.25z"
                />
              </svg>

              <p className="text-xs font-light ml-2">
                {props?.contact?.phone ? props?.contact?.phone : 'No Number'}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Card;
