import { useQuery } from 'react-query';
import Card from './Card';
import axios from 'axios';
import { addressFormType } from './AddressForm';
import Spinner from './Spinner';
import { usePopUp } from '../Context/FormContext';

interface contactListProps {
  className?: string;
}

async function getContacts() {
  const resp = await axios.get('http://127.0.0.1:8080/api/getContacts');
  return resp;
}

function ContactsList(props: contactListProps) {
  /* Remote Data handling */
  const { data, isLoading } = useQuery('get-contacts', getContacts);
  const { state } = usePopUp();

  return (
    <>
      {isLoading ? (
        <Spinner />
      ) : (
        <section
          className={`${props?.className}grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4 gap-2 grid-rows-card mx-4`}
        >
          {data?.data
            ?.filter((value: addressFormType) =>
              value.name.toLowerCase().includes(state.searchQuery.toLowerCase())
            )
            ?.sort((a: addressFormType, b: addressFormType) =>
              a.name.localeCompare(b.name)
            )
            ?.map((contact: addressFormType) => (
              <Card key={contact?._id} contact={contact} />
            ))}
        </section>
      )}
    </>
  );
}

export default ContactsList;
