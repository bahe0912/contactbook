import { usePopUp } from '../Context/FormContext';

interface buttonProps {
  className?: string;
  label: string;
  loading?: boolean;
  type?: 'submit';
  serve?: 'form';
  onClick?: () => void;
}
function Button(props: buttonProps) {
  const { handlers } = usePopUp();
  return (
    <button
      className={`${props?.className} w-[144px] h-[40px] bg-primary hover:bg-btnHover text-bgMain font-semibold rounded-button drop-shadow-lg font-Lato`}
      onClick={() => handlers?.setPopUp(true)}
      type={props?.type && props?.type}
      disabled={props?.loading}
    >
      {props?.serve === 'form' ? (
        props?.loading ? (
          <div className="flex justify-center items-center h-full w-full p-2">
            <svg
              className="animate-spin h-4 w-4  text-white "
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
            >
              <circle
                className="opacity-25"
                cx="12"
                cy="12"
                r="10"
                stroke="currentColor"
                strokeWidth="4"
              ></circle>
              <path
                className="opacity-75"
                fill="currentColor"
                d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.294a7.962 8.29 0 01-1.976-1.016A6.985 8.985 0 014.148 11H2v2h4v-1.706zM17.952 11H22v-2h-4.148a6.978 8.978 0 01-1.876 3.278 8.29 0 01-1.016 1.976L17.952 11z"
              ></path>
            </svg>
            <p className="ml-3 font-light tracking-wider">Loading....</p>
          </div>
        ) : (
          props?.label
        )
      ) : (
        props?.label
      )}
    </button>
  );
}

export default Button;
