import { QueryClient, QueryClientProvider } from 'react-query';
import { HomePage } from './screens';
import { FormContextProvider } from './Context/FormContext';
import { ToastContainer } from 'react-toastify';
const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <FormContextProvider>
        <HomePage />
        <ToastContainer />
      </FormContextProvider>
    </QueryClientProvider>
  );
}

export default App;
