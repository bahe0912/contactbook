/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        bgMain: '#FFFEFF',
        bg: '#F8F8F8',
        btnHover: '#1858FF',
        primary: '#0046FE',
        inputfield: 'rgba(0,70,254,0.3)',
        label: '#2D437E',
        error: '#EB5757',
      },
      gridTemplateColumns: {
        layout: '88px 1fr',
        list: '5vh 1fr 20vh',
        card: '20% 1fr',
        cardInner: '30% 1fr',
        'popup-layout': 'min-content 1fr',
        'popup-lg': '28vw 1fr',
        'popup-xl': '24vw 1fr',
        'popup-2xl': '16vw 1fr',
      },
      gridTemplateRows: {
        list: '5vh 10vh 1vh 1fr',
        card: 'repeat(10, minmax(0, 1fr))',
      },
      borderRadius: {
        button: '4px',
      },
      boxShadow: {
        main: '10px 40px 15px 0px rgba(0,0,0)',
        popup: '0px 40px 40px 0px rgba(0,70,254,0.3)',
      },
      fontFamily: {
        Lato: ['Lato', 'sans-serif'],
        Fira: ['Fira Sans', 'sans-serif'],
      },
    },
  },
  plugins: [],
};
